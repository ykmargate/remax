<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class BookTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/books');

        $response->assertStatus(200);
    }

    /**
     * A basic DB test example.
     *
     * @return void
     */
    public function testBooksTableHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns('books', [
                'id','title', 'subtitle', 'isbn13', 'price', 'image', 'url'
            ]), 1);
    }

}
