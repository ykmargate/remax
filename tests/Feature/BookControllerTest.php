<?php

namespace Tests\Feature;

use App\Book;
use App\Http\Controllers\BookController;
//use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class BookControllerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAddNewBook()
    {
        $request = Request::create('/books', 'POST',[
            //'id' =>1,
            'title' => 'New title',
            'subtitle' => 'New subtitle',
            'isbn13' => '1234567890123',
            'price' => '15',
            'image' => 'image',
            'url' => 'url',
        ]);
        $controller = new BookController();
        $response = $controller->store($request);
        $this->assertEquals('New book added to reading list', $response);
        $book = Book::where('title', 'New title')->delete();
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAddDuplicatedBook()
    {
        $request = Request::create('/books', 'POST',[
            'title' => 'New title',
            'subtitle' => 'New subtitle',
            'isbn13' => '1234567890123',
            'price' => '15',
            'image' => 'image',
            'url' => 'url',
        ]);
        $controller = new BookController();
        $response = $controller->store($request);
        $response = $controller->store($request);
        $this->assertEquals('Duplicate entry', substr($response,0 , 15));
        $book = Book::where('title', 'New title')->delete();
    }
}
