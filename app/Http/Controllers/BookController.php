<?php

namespace App\Http\Controllers;

use App\Book;
use Illuminate\Http\Request;
//use Request;
use GuzzleHttp\Client;
use Illuminate\Database\QueryException;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //dd($request->input('sort'));
        if($request->has('sort')){
            switch ($request->input('sort')){
                case 'title_asc':
                    $books = Book::orderBy('title')->get();
                    break;
                case 'title_desc':
                    $books = Book::orderByDesc('title')->get();
                    break;
                case 'price_asc':
                    $books = Book::orderBy('price')->get();
                    break;
                case 'price_desc':
                    $books = Book::orderByDesc('price')->get();
                    break;
            }
        }
        else {
            $books = Book::all();
        }


        return view('books.index',compact('books'))
            ->with('i', (request()->input('page', 1) - 1) * 5)
            ->with('sort', $request->input('sort'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $book = (object)['title' => '', 'price' => ''];
        return view('books.search',compact('book'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Book::create($request->all());
        }
        catch (QueryException $exception){
            $errorCode = $exception->errorInfo[1];
            if($errorCode == 1062){
                return "Duplicate entry '9780071666596' for key 'isbn13'";
            }
        }
        return 'New book added to reading list';
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function show(Book $book)
    {
        return view('books.show',compact('book'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function edit(Book $book)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Book $book)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Book  $book
     * @return \Illuminate\Http\Response
     */
    public function destroy(Book $book)
    {
        $book->delete();
        return redirect()->route('books.index')
            ->with('success','Book deleted successfully');
    }

}
