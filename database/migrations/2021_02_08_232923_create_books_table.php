<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->string('subtitle');
            $table->string('authors');
            $table->string('publisher');
            $table->string('language');
            $table->string('isbn10');
            $table->string('isbn13');
            $table->string('pages');
            $table->string('year');
            $table->string('rating');
            $table->string('desc');
            $table->string('price');
            $table->string('image');
            $table->string('url');
            $table->string('pdf');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
