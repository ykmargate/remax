@extends('books.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Search books for reading</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('books.index') }}"> Back</a>
            </div>
        </div>
    </div>

    <div class="input-group col-5" style="padding-left: 0px;padding-bottom: 5px;">
        <input type="text" class="form-control" placeholder="Search..." name="search">
        <div class="input-group-append">
            <button type="button" class="btn btn-secondary" id="btn-search">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
    <div id="pages" style="margin-bottom: 10px; margin-top: 5px;"></div>
    <table class="table table-bordered" id="table">
        <tr>
            <th width="20px">No</th>
            <th>Title</th>
            <th width="40px">Price</th>
            <th width="100px">Action</th>
        </tr>
            <tr>
                <td colspan="4">Fill search field and push "Search" button</td>
            </tr>
    </table>

    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">The Book</h4>
                    <!-- button type="button" class="close" data-dismiss="modal">&times;</button -->
                </div>

                <!-- Modal body -->
                <div class="modal-body text-center" id="modal-body">
                    <p id="book-name"></p>
                    <p style="color: green; font-weight: bold" id="book-msg">has been added</p>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay"></div>
    <style>
        .overlay{
            display: none;
            position: fixed;
            width: 100%;
            height: 100%;
            top: 0;
            left: 0;
            z-index: 999;
            background: rgba(255,255,255,0.8) url("/images/loader.gif") center no-repeat;
        }
        /*
        body{
            text-align: center;
        }
        */
        /* Turn off scrollbar when body element has the loading class */
        body.loading{
            overflow: hidden;
        }
        /* Make spinner image visible when body element has the loading class */
        body.loading .overlay{
            display: block;
        }
    </style>

    <script type="application/javascript">
        $(document).ready(function(){
            var page = 1;
            // spinner
            $(document).on({
                ajaxStart: function(){
                $("body").addClass("loading");
            },
            ajaxStop: function(){
                $("body").removeClass("loading");
            }
        });
            $(document).on('keypress',function(e) {
                if(e.which == 13) {
                    //alert('You pressed enter!');
                    $('#btn-search').trigger("click");
                }
            });
            function booksSearch(){
                let url;
                url = '/api/search?search=' + $("input[name=search]").val() + '/' + page;
                $.get(url, function(data, status){
                    //alert("Data: " + data + "\nStatus: " + status);
                    let response = JSON.parse(data);
                    page = parseInt(response.page, 10);
                    let pages = Math.ceil(response.total/10);
                    let prev, next = 'active';
                    if(page == 1) prev = 'disabled';
                    if(page == pages) next = 'disabled';
                    let table = '' +
                        '<tr>' +
                        '<th width="20px">No</th>' +
                        '<th>Title</th>' +
                        '<th width="40px">Price</th>' +
                        '<th width="100px">Actions</th>' +
                        '</tr>';
                    $.each(response.books, function(i, item) {
                        table = table + '<tr><td>' + ((page -1)*10 + i + 1) + '</td>';
                        table = table + '<td>' + item.title + '</td>';
                        table = table + '<td>' + item.price + '</td>';
                        table = table + '<td>' + '<button class="btn btn-info btn-sm" id="btn_add_' + i + '">Add</button>' + '</td>';
                        table = table + '</tr>';
                    });
                    $('#pages').html('<b>Page ' + page + ' of ' + pages +
                        '</b> <span style="float: right; font-weight: bold">' +
                        '<nav class="nav" style=" height: 24px">' +
                        '<a id="prev" href="" class="nav-link ' + prev + '" style=" height: 24px; padding: 0rem 1rem;">Previous</a>' +
                        '<a id="next" href="" class="nav-link ' + next +'" style=" height: 24px; padding: 0rem 1rem;">Next</a>' +
                        '</nav></span>');
                    $('#table').html(table);

                    $('[id^=btn_add]').on('click', function(event){
                        event.preventDefault();
                        let i = $(this).attr('id');
                        i = i.substring(8);
                        console.log('i='+i);
                        $.post("/books",
                            {
                                'title': response.books[i].title,
                                'subtitle': response.books[i].subtitle,
                                'price': response.books[i].price.substring(1),
                                '_token': '{{ csrf_token() }}',
                                'isbn13': response.books[i].isbn13,
                                'image': response.books[i].image,
                                'url': response.books[i].url,
                            },
                            function(data, status){
                                //alert(data);
                                $("#book-name").text(response.books[i].title);
                                $("#book-msg").text(data);
                                if(data == 'New book added to reading list'){
                                    $("#book-msg").css("color", "green")
                                    console.log('true-'+data+'-');
                                }
                                else {
                                    $("#book-msg").css("color", "red")
                                    console.log('false-'+data+'-');
                                }
                                $('#myModal').modal('show');
                                setTimeout(function() {$('#myModal').modal('hide');}, 4000);
                            });
                    });
                    $('#prev').on('click', function(e){
                       e.preventDefault();
                       page = page - 1;
                       booksSearch();
                    });
                    $('#next').on('click', function(e){
                        e.preventDefault();
                        page = page + 1;
                        booksSearch();
                    });

                });// end of $.get
            }

            $('#btn-search').on('click', function(){
                page = 1;
                booksSearch();
            });

        });
    </script>
@endsection
