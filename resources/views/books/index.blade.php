@extends('books.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Books for reading</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('books.create') }}">Add Book</a>
            </div>
        </div>
    </div>
    <div class="form-group form-inline"style="padding-left: 0px;padding-bottom: 5px; margin-bottom: 0px;">
        <label for="FormControlSelect">Order by:&nbsp;</label>
        <select class="form-control" id="FormControlSelect">
            <option value="">--- Sort order ---</option>
            <option value="title_asc" @if($sort == 'title_asc'){{'selected'}}@endif>Title: Ascending</option>
            <option value="title_desc" @if($sort == 'title_desc'){{'selected'}}@endif>Title: Descending</option>
            <option value="price_asc" @if($sort == 'price_asc'){{'selected'}}@endif>Price: Low to High</option>
            <option value="price_desc" @if($sort == 'price_desc'){{'selected'}}@endif>Price: High to Low</option>
        </select>
    </div>
    <table class="table table-bordered">
        <tr>
            <th width="20px">No</th>
            <th>Title</th>
            <th width="40px">Price</th>
            <th width="180px">Actions</th>
        </tr>
        @foreach ($books as $book)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $book->title }}</td>
                <td>${{ $book->price }}</td>
                <td>
                    <form action="{{ route('books.destroy',$book->id) }}" method="POST" style="margin-block-end: 0em;">
                        <a class="btn btn-info btn-sm" href="{{ route('books.show',$book->id) }}">Details</a>
                        <!--<a class="btn btn-primary" href="{{ route('books.edit',$book->id) }}">Edit</a>-->
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    <script type="application/javascript">
        $(document).ready(function(){
            if ($('#msg').length)
            {
                $("#msg").delay(5000).fadeOut('slow');
            }
            $("#FormControlSelect").on('change', function(){
                //alert($(this).find('option:selected').val());
                let url = '/books?sort=' + $(this).find('option:selected').val();
                window.location.replace(url);
            });
        });
    </script>
@endsection
