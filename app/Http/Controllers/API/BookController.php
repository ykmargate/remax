<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
//use Request;
use GuzzleHttp\Client;
use Illuminate\Database\QueryException;

class BookController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchBooks(Request $request)
    {
        $url = 'https://api.itbook.store';
        $client = new Client(['base_uri' => $url]);
        $response = $client->request('GET', '/1.0/search/' . $request->search);
        //echo $response->getBody();
        //$arr = json_decode($response->getBody());
        return($response->getBody());
    }

}
