<?php

namespace Tests\Unit;

//use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
//use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;

class BookModelTest extends TestCase
{
    //use RefreshDatabase;

    /** @test  */
    public function testBooksTableHasExpectedColumns()
    {
        $this->assertTrue(
            Schema::hasColumns('books', [
                'id','title', 'subtitle', 'isbn13', 'price', 'image', 'url'
            ]), 1);
    }
}


