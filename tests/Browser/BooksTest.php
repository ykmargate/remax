<?php

namespace Tests\Browser;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class BooksTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        $this->browse(function (Browser $browser) {
            //$this->assertTrue(true);
            /*
            $browser->visit('/')
                ->pause(5000)
                ->screenshot('home');

            $browser->visit('/')
                ->assertSee('Laravel');
            */

            $browser->visit('/books')
                ->pause(5000)
                ->screenshot('home')

            ->assertSee('Books for reading');
            $browser->visit('/books/19')
                ->assertSee('Show Book');
            $browser->visit('/books/create')
                ->assertSee('Search books for reading');
            $browser->type('search', 'MongoDB')
                ->press('#btn-search')
                ->pause(5000)
                ->screenshot('search-Mongo')
                ->assertSee('MongoDB Cookbook');

        });
    }
}
